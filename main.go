package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
	"zendesk/app"
	"zendesk/transport"
)

var c = app.NewAppServiceComponent()

func GetPath(w http.ResponseWriter, r *http.Request) {
	dec := json.NewDecoder(r.Body)
	var req transport.GetRouteRequest
	err := dec.Decode(&req)
	if err != nil {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	path, err := c.FindPath(req.Start, req.End)
	if err != nil {
		fmt.Fprintf(w, "%s", err.Error())
	} else {
		fmt.Fprintf(w, path.String())
	}
}

func GetPathWithTime(w http.ResponseWriter, r *http.Request) {
	dec := json.NewDecoder(r.Body)
	var req transport.GetRouteRequest
	err := dec.Decode(&req)
	if err != nil {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	layout := "2006-01-02T15:04"
	t, err := time.Parse(layout, req.Time)
	if err != nil {
		fmt.Fprintf(w,"please us this time format as an input (\"YYYY-MM-DDThh:mm\" format, e.g. '2019-01-31T16:00')")
		return
	}
	result, err := c.FindPathAtTime(req.Start, req.End, t)
	if err != nil {
		fmt.Fprintf(w, err.Error())
	} else {
		fmt.Fprintf(w, result.String())
	}
}

func main() {
	time.Local = time.UTC
	r := mux.NewRouter()
	r.HandleFunc("/getPath", GetPath).Methods("POST")
	r.HandleFunc("/getPathWithTime", GetPathWithTime).Methods("POST")
	r.Use(app.AcceptJsonOnly)
	srv := &http.Server{
		Handler: r,
		Addr: "127.0.0.1:8081",
		WriteTimeout: 15 * time.Second,
		ReadTimeout: 15 * time.Second,
	}
	fmt.Printf("Server starting at port :8081\n")
	log.Fatal(srv.ListenAndServe())
}
