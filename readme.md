# Zendesk MRT Routing Exercise

An implementation of zendesk mrt routing exercise using golang. This implementation supporting two service for finding route
1. /getPath
2. /getPathWithTime

The MRT Map is build based from `data/StationMap.csv` data with these assumptions:
1. Every station with the same line that adjacent is consider to be connected for example [ EW1, EW2, EW3, CC1] meaning EW1 is connected to EW2, EW2 is connected to EW1 & EW3, EW3 is connected to EW2.
2. Every station that share a same name it's connected.
3. Opening date in `data/StationMap.csv` it's not being consider in this implementation.
4. The different delay time for peak hour, night-time, etc. Is being take account in this implementation.
```
Peak hours (6am-9am and 6pm-9pm on Mon-Fri)
	NS and NE lines take 12 minutes per station
	All other train lines take 10 minutes
	Every train line change adds 15 minutes of waiting time to the journey


Night hours (10pm-6am on Mon-Sun)
	DT, CG and CE lines do not operate
	TE line takes 8 minutes per stop
	All trains take 10 minutes per stop
	Every train line change adds 10 minutes of waiting time to the journey


Non-Peak hours (all other times)
	DT and TE lines take 8 minutes per stop
	All trains take 10 minutes per stop
	Every train line change adds 10 minutes of waiting time to the journey
``` 

# Code Structure

- app
	 - solution
		 - test
		 - entity.go
		 - helper.go
		 - p_queue.go
		 - solution.go
	 - component.go
	 - filter.go
 - data
 - transport
 - main.go

# Strategy to Find the Route
The approach to finding route is using implementation of [Dijkstra's algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm).

# prerequisite
1. Go v 1.15 Please follow this installation [doc](https://golang.org/doc/install)
# Installation
1. `go install ./...`
# Running
1. `go run main.go`

you should see `Server starting at port :8081` appear in your terminal

```
if you find problem to run the program you may run the binary file included in the project
./main
```
# /getPath
Request Example:
```
{
	"start": "Boon Lay",
	"end": "Little India"
}

please set the `content-type` header as `application/json` in your request
station name is case sensitive so `redhill` != `Redhill`
```

curl example:
`curl -X POST \
  http://localhost:8081/getPath \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"start": "Boon Lay",
	"end": "Little India"
}'`

Response Example:
```
Distance from Boon Lay to Little India is 14 stations

Route: ('EW27','EW26','EW25','EW24','EW23','EW22','EW21','CC22','CC21','CC20','CC19','DT9','DT10','DT11','DT12')

Take EW line from Boon Lay to Lakeside
Take EW line from Lakeside to Chinese Garden
Take EW line from Chinese Garden to Jurong East
Take EW line from Jurong East to Clementi
Take EW line from Clementi to Dover
Take EW line from Dover to Buona Vista
Change from EW line to CC line
Take CC line from Buona Vista to Holland Village
Take CC line from Holland Village to Farrer Road
Take CC line from Farrer Road to Botanic Gardens
Change from CC line to DT line
Take DT line from Botanic Gardens to Stevens
Take DT line from Stevens to Newton
Take DT line from Newton to Little India
```

# /getPathWithTime
Request Example:
```
{
	"start": "Boon Lay",
	"end": "Little India",
	"time": "2006-01-22T07:04" // date format YYYY-MM-DDThh:mm
}

please set the `content-type` header as `application/json` in your request
station name is case sensitive so `redhill` != `Redhill`
```

curl Example:
`curl -X POST \
  http://localhost:8081/getPath \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"start": "Boon Lay",
	"end": "Little India",
	"time": "2006-01-22T07:04"
}'`

Response example:
```
It's require 134 minutes to travel from Boon Lay to Little India at 2006-01-22 07:04:00 +0000 UTC
Route: ('EW27','EW26','EW25','EW24','EW23','EW22','EW21','CC22','CC21','CC20','CC19','DT9','DT10','DT11','DT12')

Take EW line from Boon Lay to Lakeside
Take EW line from Lakeside to Chinese Garden
Take EW line from Chinese Garden to Jurong East
Take EW line from Jurong East to Clementi
Take EW line from Clementi to Dover
Take EW line from Dover to Buona Vista
Change from EW line to CC line
Take CC line from Buona Vista to Holland Village
Take CC line from Holland Village to Farrer Road
Take CC line from Farrer Road to Botanic Gardens
Change from CC line to DT line
Take DT line from Botanic Gardens to Stevens
Take DT line from Stevens to Newton
Take DT line from Newton to Little India
```
# Testing
1. `go test ./... -v`

# Troubleshooting
If there's any problem to run or any question about the implementation you can reach me through this email tracy.ridwan@gmail.com