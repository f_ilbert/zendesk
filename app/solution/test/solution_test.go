/*

 */
package test

import (
	"testing"
	"time"
	"zendesk/app/solution"
)

var sol = solution.NewSolution()
func TestFindPath1(t *testing.T) {
	start := "random_name"
	end := "random_name"
	_, err := sol.FindPath(start, end)
	if err == nil {
		t.Errorf("Expected to be error due invalid start station name")
	}
}

func TestFindPath2(t *testing.T) {
	start := "Redhill"
	end := "random_name"
	_, err := sol.FindPath(start, end)
	if err == nil {
		t.Errorf("Expected to be error due invalid end station name")
	}
}

func TestFindPath3(t *testing.T) {
	start := "Redhill"
	end := "Tiong Bahru"
	res, err := sol.FindPath(start, end)
	if err != nil {
		t.Errorf("got err: %s", err.Error())
	}

	if len(res.Path) != 1 {
		t.Errorf("expected pathLen: %v; got: %v", 1, len(res.Path))
	}

	if res.Path[0].Start.Name != "Redhill" {
		t.Errorf("expected pathLen: Redhill; got: %v",  res.Path[0].Start.Name)
	}

	if res.Path[0].End.Name != "Tiong Bahru" {
		t.Errorf("expected pathLen: Tiong Bahru; got: %v",  res.Path[0].End.Name)
	}

	if len(res.Route) != 2 {
		t.Errorf("expected routeLen: %v; got: %v", 2, len(res.Route))
	}

	if res.Route[1].Code != "EW18" {
		t.Errorf("expected pathLen: EW18; got: %v",  res.Route[0].Code)
	}

	if res.Route[0].Code != "EW17" {
		t.Errorf("expected pathLen: EW17; got: %v",  res.Route[0].Code)
	}
}

func TestFindPath4(t *testing.T) {
	start := "Redhill"
	end := "Redhill"
	res, err := sol.FindPath(start, end)
	if err != nil {
		t.Errorf("got err: %s", err.Error())
	}

	if len(res.Path) != 0 {
		t.Errorf("expected pathLen: %v; got: %v", 0, len(res.Path))
	}

	if len(res.Route) != 1 {
		t.Errorf("expected routeLen: %v; got: %v", 1, len(res.Route))
	}

	if res.Route[0].Code != "EW18" {
		t.Errorf("expected pathLen: EW18; got: %v",  res.Route[0].Code)
	}
}

func TestFindPath5(t *testing.T) {
	start := "Boon Lay"
	end := "Little India"
	res, err := sol.FindPath(start, end)
	if err != nil {
		t.Errorf("got err: %s", err.Error())
	}

	path := []struct{
		Start string
		End string
	} {
		{
			Start: "DT11",
			End: "DT12",
		},
		{
			Start: "DT10",
			End: "DT11",
		},
		{
			Start: "DT9",
			End: "DT10",
		},
		{
			Start: "CC19",
			End: "DT9",
		},
		{
			Start: "CC20",
			End: "CC19",
		},
		{
			Start: "CC21",
			End: "CC20",
		},
		{
			Start: "CC22",
			End: "CC21",
		},
		{
			Start: "EW21",
			End: "CC22",
		},
		{
			Start: "EW22",
			End: "EW21",
		},
		{
			Start: "EW23",
			End: "EW22",
		},
		{
			Start: "EW24",
			End: "EW23",
		},
		{
			Start: "EW25",
			End: "EW24",
		},
		{
			Start: "EW26",
			End: "EW25",
		},
		{
			Start: "EW27",
			End: "EW26",
		},
	}
	if len(res.Path) != 14 {
		t.Errorf("expected pathLen: %v; got: %v", 14, len(res.Path))
	}
	for i,v := range res.Path {
		if v.Start.Code != path[i].Start {
			t.Errorf("expected edge[%d].start: %v; got: %v", i, path[i].Start, v.Start.Code)
		}

		if v.End.Code != path[i].End {
			t.Errorf("expected edge[%d].end: %v; got: %v", i, path[i].End, v.End.Code)
		}
	}

	if len(res.Route) != 15 {
		t.Errorf("expected routeLen: %v; got: %v", 15, len(res.Route))
	}
	route := []string{
		"EW27","EW26","EW25","EW24","EW23","EW22","EW21","CC22","CC21","CC20","CC19","DT9","DT10","DT11","DT12",
	}
	for i, _ := range res.Route {
		n := len(res.Route)
		if res.Route[n-i-1].Code != route[i] {
			t.Errorf("expected route[%d]: %v; got: %v", i, route[i], res.Route[n-i-1].Code)
		}
	}
}

func TestGetPathWithTime(t *testing.T) {
	time.Local = time.UTC

	start := "Boon Lay"
	end := "Little India"
	startTime := time.Date(2020, 1, 22, 12,15,0,0,time.UTC)
	res, err := sol.FindPathAtTime(start, end, startTime)
	if err != nil {
		t.Errorf("got err: %s", err.Error())
	}

	path := []struct{
		Start string
		End string
	} {
		{
			Start: "DT11",
			End: "DT12",
		},
		{
			Start: "DT10",
			End: "DT11",
		},
		{
			Start: "DT9",
			End: "DT10",
		},
		{
			Start: "CC19",
			End: "DT9",
		},
		{
			Start: "CC20",
			End: "CC19",
		},
		{
			Start: "CC21",
			End: "CC20",
		},
		{
			Start: "CC22",
			End: "CC21",
		},
		{
			Start: "EW21",
			End: "CC22",
		},
		{
			Start: "EW22",
			End: "EW21",
		},
		{
			Start: "EW23",
			End: "EW22",
		},
		{
			Start: "EW24",
			End: "EW23",
		},
		{
			Start: "EW25",
			End: "EW24",
		},
		{
			Start: "EW26",
			End: "EW25",
		},
		{
			Start: "EW27",
			End: "EW26",
		},
	}
	if len(res.Path) != 14 {
		t.Errorf("expected pathLen: %v; got: %v", 14, len(res.Path))
	}
	for i,v := range res.Path {
		if v.Start.Code != path[i].Start {
			t.Errorf("expected edge[%d].start: %v; got: %v", i, path[i].Start, v.Start.Code)
		}

		if v.End.Code != path[i].End {
			t.Errorf("expected edge[%d].end: %v; got: %v", i, path[i].End, v.End.Code)
		}
	}

	if len(res.Route) != 15 {
		t.Errorf("expected routeLen: %v; got: %v", 15, len(res.Route))
	}
	route := []string{
		"EW27","EW26","EW25","EW24","EW23","EW22","EW21","CC22","CC21","CC20","CC19","DT9","DT10","DT11","DT12",
	}
	for i, _ := range res.Route {
		n := len(res.Route)
		if res.Route[n-i-1].Code != route[i] {
			t.Errorf("expected route[%d]: %v; got: %v", i, route[i], res.Route[n-i-1].Code)
		}
	}

	if res.TravelTime != 134 {
		t.Errorf("expected travelTime: 134; got %v", res.TravelTime)
	}
}