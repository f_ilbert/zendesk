package test

import (
	"fmt"
	"testing"
	"time"
	"zendesk/app/solution"
)

// ALL TEST SCENARIO ARE USING THIS DATE
// 4 JAN 2021 00:00 ~ 10 JAN 2021 23:59
/*
Peak hours (6am-9am and 6pm-9pm on Mon-Fri)
	NS and NE lines take 12 minutes per station
	All other train lines take 10 minutes
	Every train line change adds 15 minutes of waiting time to the journey


Night hours (10pm-6am on Mon-Sun)
	DT, CG and CE lines do not operate
	TE line takes 8 minutes per stop
	All trains take 10 minutes per stop
	Every train line change adds 10 minutes of waiting time to the journey


Non-Peak hours (all other times)
	DT and TE lines take 8 minutes per stop
	All trains take 10 minutes per stop
	Every train line change adds 10 minutes of waiting time to the journey
 */

// TEST IS PEAK HOUR THROUGH 1 WEEK DATE
func TestIsPeakHour(t *testing.T) {
	type tCase struct {
		T        time.Time
		CaseName string
		Expected bool
	}
	cases := make([]tCase, 0)
	tcCheck := make(map[int64]bool)
	// GENERATE TC FOR MONDAY~FRIDAY at 6AM to 9 AM
	for day := 4; day <= 8; day++ {
		for hour := 6; hour <9; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				cases = append(cases, tCase{
					T:        t,
					CaseName: fmt.Sprintf("isPeakHour at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
					Expected: true,
				})
				tcCheck[t.Unix()] = true
			}
		}
	}

	// GENERATE TC FOR MONDAY~FRIDAY at 6PM to 9 PM
	for day := 4; day <= 8; day++ {
		for hour := 18; hour <21; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				cases = append(cases, tCase{
					T:        t,
					CaseName: fmt.Sprintf("isPeakHour at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
					Expected: true,
				})
				tcCheck[t.Unix()] = true
			}
		}
	}

	// GENERATE TC FOR THE REST OF THE TIME OF THE WEEK
	for day := 4; day <= 10; day++ {
		for hour := 0; hour <=23; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				if _, ok := tcCheck[t.Unix()]; !ok {
					cases = append(cases, tCase{
						T:        t,
						CaseName: fmt.Sprintf("isPeakHour at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
						Expected: false,
					})
					tcCheck[t.Unix()] = true
				}
			}
		}
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%s", tc.CaseName), func (t *testing.T) {
			r := solution.IsPeakHour(tc.T)
			if r != tc.Expected {
				t.Errorf("got %v; want %v", r, tc.Expected)
			}
		})
	}
}

// TEST IS NIGHT HOUR THROUGH THE DATE 4 JAN 2021 00:00 UNTIL 10 JAN 2021 23:59
func TestIsNightHour(t *testing.T) {
	type tCase struct {
		T        time.Time
		CaseName string
		Expected bool
	}
	cases := make([]tCase, 0)
	tcCheck := make(map[int64]bool)
	// GENERATE TC FOR MONDAY~SUNDAY at 10PM to 23:59 PM
	for day := 4; day <= 10; day++ {
		for hour := 22; hour <=23; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				cases = append(cases, tCase{
					T:        t,
					CaseName: fmt.Sprintf("isNightHour at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
					Expected: true,
				})
				tcCheck[t.Unix()] = true
			}
		}
	}

	// GENERATE TC FOR MONDAY~SUNDAY at 00:00AM to 06:00 AM
	for day := 4; day <= 10; day++ {
		for hour := 0; hour <6; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				cases = append(cases, tCase{
					T:        t,
					CaseName: fmt.Sprintf("isNightHour at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
					Expected: true,
				})
				tcCheck[t.Unix()] = true
			}
		}
	}

	// GENERATE TC FOR THE REST OF THE TIME OF THE WEEK
	for day := 4; day <= 10; day++ {
		for hour := 0; hour <=23; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				if _, ok := tcCheck[t.Unix()]; !ok {
					cases = append(cases, tCase{
						T:        t,
						CaseName: fmt.Sprintf("isNightHour at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
						Expected: false,
					})
					tcCheck[t.Unix()] = true
				}
			}
		}
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%s", tc.CaseName), func (t *testing.T) {
			r := solution.IsNightHour(tc.T)
			if r != tc.Expected {
				t.Errorf("got %v; want %v", r, tc.Expected)
			}
		})
	}
}

func testGetTravelTime(t *testing.T, PeakHourDelay int64 , NightHourDelay int64, NormalDelay int64, s solution.Station, e solution.Station) {
	type tCase struct {
		T        time.Time
		CaseName string
		Expected time.Time
	}
	cases := make([]tCase, 0)
	tcCheck := make(map[int64]bool)

	//// GENERATE TC FOR MONDAY~FRIDAY at 6AM to 9 AM
	for day := 4; day <= 8; day++ {
		for hour := 6; hour <9; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				cases = append(cases, tCase{
					T:        t,
					CaseName: fmt.Sprintf("getTravelTime at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
					Expected: t.Add(time.Duration(PeakHourDelay)*time.Minute),
				})
				tcCheck[t.Unix()] = true
			}
		}
	}

	// GENERATE TC FOR MONDAY~FRIDAY at 6PM to 9 PM
	for day := 4; day <= 8; day++ {
		for hour := 18; hour <21; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				cases = append(cases, tCase{
					T:        t,
					CaseName: fmt.Sprintf("getTravelTime at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
					Expected: t.Add(time.Duration(PeakHourDelay)*time.Minute),
				})
				tcCheck[t.Unix()] = true
			}
		}
	}

	// GENERATE TC FOR MONDAY~SUNDAY at 10PM to 23:59 PM
	for day := 4; day <= 10; day++ {
		for hour := 22; hour <=23; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				_t := time.Time{}
				if NightHourDelay == -1 {
					_t = time.Unix(0, 0)
				} else {
					_t = t.Add(time.Duration(NightHourDelay)*time.Minute)
				}
				cases = append(cases, tCase{
					T:        t,
					CaseName: fmt.Sprintf("getTravelTime at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
					Expected: _t,
				})
				tcCheck[t.Unix()] = true
			}
		}
	}

	// GENERATE TC FOR MONDAY~SUNDAY at 00:00AM to 06:00 AM
	for day := 4; day <= 10; day++ {
		for hour := 0; hour <6; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				_t := time.Time{}
				if NightHourDelay == -1 {
					_t = time.Unix(0, 0)
				} else {
					_t = t.Add(time.Duration(NightHourDelay)*time.Minute)
				}
				cases = append(cases, tCase{
					T:        t,
					CaseName: fmt.Sprintf("getTravelTime at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
					Expected: _t,
				})
				tcCheck[t.Unix()] = true
			}
		}
	}

	// GENERATE TC FOR THE REST OF THE TIME OF THE WEEK
	for day := 4; day <= 10; day++ {
		for hour := 0; hour <=23; hour++ {
			for minute := 0; minute <= 59; minute++ {
				t := time.Date(2021, 01, day, hour, minute, 0, 0, time.UTC)
				if _, ok := tcCheck[t.Unix()]; !ok {
					cases = append(cases, tCase{
						T:        t,
						CaseName: fmt.Sprintf("getTravelTime at %s, %d:%.2d AM", t.Weekday().String(), t.Hour(), t.Minute()),
						Expected: t.Add(time.Duration(NormalDelay)*time.Minute),
					})
					tcCheck[t.Unix()] = true
				}
			}
		}
	}

	for _, tc := range cases {
		t.Run(fmt.Sprintf("%s Line to %s line %s", s.LineName,e.LineName, tc.CaseName), func (t *testing.T) {
			r := solution.GetTravelTime(s, e, tc.T)
			if tc.Expected.Unix() == 0 {
				if r != nil {
					t.Errorf("got %v; want NIL", r.String())
				}
			} else if r.Unix() != tc.Expected.Unix() {
				t.Errorf("got %v; want %v", r.String(), tc.Expected.String())
			}
		})
	}
}

func TestGetTravelTime(t *testing.T) {
	for k, v := range solution.LineMap {
		s := solution.Station{
			LineName: k,
		}
		testGetTravelTime(t, v.PeakHourDelay, v.NightHourDelay, v.NonPeakHourDelay, s, s)
	}

	testGetTravelTime(t, 15, 10, 10, solution.Station{
		LineName: "CC",
	}, solution.Station{
		LineName: "DT",
	})
}

func testLineMapConst(t *testing.T, PeakHourDelay int64 , NightHourDelay int64, NormalDelay int64, s string) {
	delay := solution.LineMap[s]
	t.Run(fmt.Sprintf("%s LineMap PeakHourDelayTest", s), func (t *testing.T) {
		if delay.PeakHourDelay != PeakHourDelay {
			t.Errorf("got %v; want %v", delay.PeakHourDelay, PeakHourDelay)
		}
	})
	t.Run(fmt.Sprintf("%s LineMap NightHourDelayTest", s), func (t *testing.T) {
		if delay.NightHourDelay != NightHourDelay {
			t.Errorf("got %v; want %v", delay.NightHourDelay, NightHourDelay)
		}
	})
	t.Run(fmt.Sprintf("%s LineMap NormalDelay", s), func (t *testing.T) {
		if delay.NonPeakHourDelay != NormalDelay {
			t.Errorf("got %v; want %v", delay.NonPeakHourDelay, NormalDelay)
		}
	})
}

func TestNSLineMapConst(t *testing.T) {
	testLineMapConst(t, 12, 10, 10, "NS")
}

func TestEWLineMapConst(t *testing.T) {
	testLineMapConst(t, 10, 10, 10, "EW")
}

func TestCGLineMapConst(t *testing.T) {
	testLineMapConst(t, 10, -1, 10, "CG")
}

func TestCELineMapConst(t *testing.T) {
	testLineMapConst(t, 10, -1, 10, "CE")
}

func TestNELineMapConst(t *testing.T) {
	testLineMapConst(t, 12, 10, 10, "NE")
}

func TestCCLineMapConst(t *testing.T) {
	testLineMapConst(t, 10, 10, 10, "CC")
}

func TestDTLineMapConst(t *testing.T) {
	testLineMapConst(t, 10, -1, 8, "DT")
}

func TestTELineMapConst(t *testing.T) {
	testLineMapConst(t, 10, 8, 8, "TE")
}