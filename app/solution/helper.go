package solution

import (
	"time"
)

/*
Peak hours (6am-9am and 6pm-9pm on Mon-Fri)
*/
func IsPeakHour(t time.Time) bool {
	if t.Weekday() == time.Saturday || t.Weekday() == time.Sunday {
		return false
	}
	// 6AM ~ 9AM
	if 6<=t.Hour() && t.Hour() <=8 {
		return true
	}

	// 6PM ~ 9PM
	if 18<=t.Hour() && t.Hour() <21 {
		return true
	}

	return false
}

/*
Night hours (10pm-6am on Mon-Sun)
*/
func IsNightHour(t time.Time) bool {
	// 10PM~12 midnight
	if 22 <= t.Hour() && t.Hour() <= 23 {
		return true
	}
	// 00:00 am
	if t.Hour() == 0 && t.Minute() == 0 {
		return true
	}

	// 12 midnight till 5:59 am
	if 0<=t.Hour() && t.Hour()<6 {
		return true
	}

	return false
}

/*
Peak hours (6am-9am and 6pm-9pm on Mon-Fri)
	NS and NE lines take 12 minutes per station
	All other train lines take 10 minutes
	Every train line change adds 15 minutes of waiting time to the journey


Night hours (10pm-6am on Mon-Sun)
	DT, CG and CE lines do not operate
	TE line takes 8 minutes per stop
	All trains take 10 minutes per stop
	Every train line change adds 10 minutes of waiting time to the journey


Non-Peak hours (all other times)
	DT and TE lines take 8 minutes per stop
	All trains take 10 minutes per stop
	Every train line change adds 10 minutes of waiting time to the journey
*/

var LineMap = map[string]struct{
	PeakHourDelay int64
	NightHourDelay int64
	NonPeakHourDelay int64
} {
	"NS": {
		PeakHourDelay: 12,
		NightHourDelay: 10,
		NonPeakHourDelay: 10,
	},
	"EW": {
		PeakHourDelay: 10,
		NightHourDelay: 10,
		NonPeakHourDelay: 10,
	},
	"CG": {
		PeakHourDelay: 10,
		NightHourDelay: -1,
		NonPeakHourDelay: 10,
	},
	"CE": {
		PeakHourDelay: 10,
		NightHourDelay: -1,
		NonPeakHourDelay: 10,
	},
	"NE": {
		PeakHourDelay: 12,
		NightHourDelay: 10,
		NonPeakHourDelay: 10,
	},
	"CC": {
		PeakHourDelay: 10,
		NightHourDelay: 10,
		NonPeakHourDelay: 10,
	},
	"DT": {
		PeakHourDelay: 10,
		NightHourDelay: -1,
		NonPeakHourDelay: 8,
	},
	"TE": {
		PeakHourDelay: 10,
		NightHourDelay: 8,
		NonPeakHourDelay: 8,
	},

}

func GetTravelTime(start Station, end Station, t time.Time) *time.Time {
	if start.LineName == end.LineName {
		if IsPeakHour(t) {
			delay := LineMap[start.LineName].PeakHourDelay
			t = t.Add(time.Duration(delay)*time.Minute)
		} else if IsNightHour(t) {
			delay := LineMap[start.LineName].NightHourDelay
			if delay == -1 {
				return nil
			}
			t = t.Add(time.Duration(delay)*time.Minute)
		} else {
			delay := LineMap[start.LineName].NonPeakHourDelay
			t = t.Add(time.Duration(delay)*time.Minute)
		}
	} else {
		if IsPeakHour(t) {
			t = t.Add(15*time.Minute)
		} else {
			t = t.Add(10*time.Minute)
		}
	}

	return &t
}
