package solution

import (
	"container/heap"
	"fmt"
	"time"
)

type Solution struct {
	// hashtable for mapping stationCode into StationObject
	CodeToStation map[string]Station

	// Graph is an adj List that represent how's the mrt graph is structure
	// key = station name
	// []Station = List of Station that exist inside the station
	// for example
	// City Hall = [NS25, EW13]
	Graph map[string][]Station
}

func NewSolution() *Solution {
	stations := DataLoader()
	graph := StationToAdjList(stations)
	codeToStation := generateCodeStationMap(graph)
	return &Solution{
		CodeToStation: codeToStation,
		Graph: graph,
	}
}

func (sol Solution) CheckStationName(s string) error {
	_, ok := sol.Graph[s]
	if !ok {
		return fmt.Errorf("there's no station with name %s", s)
	}

	return nil
}

func (sol Solution) FindPath(start string, end string) (*FindPathResult, error) {
	err := sol.CheckStationName(start)
	if err != nil {
		return nil, err
	}

	err = sol.CheckStationName(end)
	if err != nil {
		return nil, err
	}

	pq := NewPriorityQueue()
	memo := make(map[string]int64)
	par := make(map[string]string)
	for _, s := range sol.Graph[start] {
		heap.Push(pq, &Item{
			Station: s,
			PrevStation: s,
			Time:    0,
		})
	}
	var res *Item
	res = nil
	for ;!pq.IsEmpty(); {
		now := pq.Top().(*Item)
		heap.Pop(pq)
		if cTime, ok := memo[now.Station.Code];
			!ok || cTime > now.Time {
			memo[now.Station.Code] = now.Time
			par[now.Station.Code] = now.PrevStation.Code
			if now.Station.Name == end {
				res = now
				break
			}
			for _, s := range now.Station.Next {
				st := sol.CodeToStation[s]
				next := &Item{
					Station: st,
					PrevStation: now.Station,
					Time: now.Time+1,
				}

				if _cTime, _ok := memo[next.Station.Code];
					!_ok || _cTime > next.Time {
					heap.Push(pq, next)
				}
			}
		}
	}
	if res == nil {
		return 	nil, fmt.Errorf("there's no route")
	}

	pathResult := &FindPathResult{
		Start: start,
		End: end,
	}
	now := res.Station

	for now.Code!=par[now.Code]  {
		pathResult.Route = append(pathResult.Route, now)
		end := now
		now = sol.CodeToStation[par[now.Code]]
		pathResult.Path = append(pathResult.Path, edge{
				Start: now,
				End: end,
			})
	}

	pathResult.Route = append(pathResult.Route, now)
	return pathResult, nil
}

func (sol Solution) FindPathAtTime(start string, end string, startTime time.Time) (*FindPathAtTimeResult, error) {
	err := sol.CheckStationName(start)
	if err != nil {
		return nil, err
	}

	err = sol.CheckStationName(end)
	if err != nil {
		return nil, err
	}

	pq := NewPriorityQueue()
	memo := make(map[string]int64)
	par := make(map[string]string)
	for _, s := range sol.Graph[start] {
		heap.Push(pq, &Item{
			Station:     s,
			PrevStation: s,
			Time:        startTime.Unix(),
		})
	}
	var res *Item
	res = nil
	for ;!pq.IsEmpty(); {
		now := pq.Top().(*Item)
		heap.Pop(pq)
		if cTime, ok := memo[now.Station.Code];
			!ok || cTime > now.Time {
			memo[now.Station.Code] = now.Time
			par[now.Station.Code] = now.PrevStation.Code
			if now.Station.Name == end {
				res = now
				break
			}
			for _, s := range now.Station.Next {
				st := sol.CodeToStation[s]
				t := time.Unix(now.Time, 0)
				tt := GetTravelTime(now.Station, st, t)
				if tt == nil {
					continue
				}
				t = *tt
				next := &Item{
					Station: st,
					PrevStation: now.Station,
					Time: t.Unix(),
				}

				if _cTime, _ok := memo[next.Station.Code];
					!_ok || _cTime > next.Time {
					heap.Push(pq, next)
				}
			}
		}
	}
	if res == nil {
		return 	nil, fmt.Errorf("there's no route")
	}
	pathResult := &FindPathAtTimeResult{
		Start: start,
		End: end,
		StartTime: startTime,
	}
	now := res.Station
	arrive := time.Unix(res.Time, 0)
	travelTime := int(arrive.Sub(startTime).Minutes())
	for now.Code!=par[now.Code]  {
		pathResult.Route = append(pathResult.Route, now)
		end := now
		now = sol.CodeToStation[par[now.Code]]
		pathResult.Path = append(pathResult.Path, edge{
			Start: now,
			End: end,
		})
	}

	pathResult.Route = append(pathResult.Route, now)
	pathResult.TravelTime = travelTime
	return pathResult, nil
}