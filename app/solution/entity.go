package solution

import (
	"fmt"
	"time"
)
type Station struct {
	Name string
	Code string
	LineName string
	StopNumber int
	Next []string // list of stations that connected with this stations
}

func (s Station) String() string {
	return fmt.Sprintf("Name:%s Code:%s", s.Name, s.Code)
}

type FindPathResult struct {
	Start string
	End string
	Path []edge
	Route []Station
}

func(f *FindPathResult) String() string {
	n := len(f.Path)
	result := ""
	for i:=0; i<=n; i++ {
		result += fmt.Sprintf("'%s',", f.Route[n-i].Code)
	}
	result = fmt.Sprintf("Route: (%s)\n\n", result[:len(result)-1])
	if n == 0 {
		return fmt.Sprintf("%syou're already here!",result)
	}

	for i:=0; i<n; i++ {
		result += f.Path[n-i-1].String()
	}

	return fmt.Sprintf("Distance from %s to %s is %d stations\n\n%s", f.Start, f.End, len(f.Path), result)
}

type FindPathAtTimeResult struct {
	Start string
	End string
	StartTime time.Time
	Path []edge
	Route []Station
	TravelTime int
}

func (f *FindPathAtTimeResult) String() string {
	n := len(f.Path)
	result := ""
	for i:=0; i<=n; i++ {
		result += fmt.Sprintf("'%s',", f.Route[n-i].Code)
	}
	result = fmt.Sprintf("Route: (%s)\n\n", result[:len(result)-1])
	if n == 0 {
		return fmt.Sprintf("%syou're already here!",result)
	}

	for i:=0; i<n; i++ {
		result += f.Path[n-i-1].String()
	}

	return fmt.Sprintf("It's require %d minutes to travel from %s to %s at %s\n%s", f.TravelTime, f.Start, f.End, f.StartTime.String(), result)
}

type edge struct {
	Start Station
	End Station
}

func (e edge) String() string{
	if e.Start.LineName == e.End.LineName {
		return fmt.Sprintf("Take %s line from %s to %s\n", e.Start.LineName, e.Start.Name, e.End.Name)
	} else {
		return fmt.Sprintf("Change from %s line to %s line\n", e.Start.LineName, e.End.LineName)
	}
}