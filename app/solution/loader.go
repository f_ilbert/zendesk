package solution

import (
	"encoding/csv"
	"io"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strconv"
)
/*
	This loader functions is responsible to transform the csv file into a graph representation for the solution.go
 */

func DataLoader() []Station {
	stations := make([]Station, 0)
	fp, _ := filepath.Abs("data/StationMap.csv")
	file, err := os.Open(fp)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	r := csv.NewReader(file)
	for {
		row, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		lineName, stopNumber := extractCode(row[0])
		stations = append(stations, Station{
			Name: row[1],
			Code: row[0],
			LineName: lineName,
			StopNumber: stopNumber,
			Next: []string{},
		})
	}

	return stations
}

func extractCode(code string) (string, int) {
	lineName := code[0:2]
	code = code[2:]
	stopNumber, _ := strconv.Atoi(code)

	return lineName, stopNumber
}

func StationToAdjList(stations []Station) map[string][]Station {
	sort.Slice(stations, func(i, j int) bool {
		if stations[i].LineName == stations[j].LineName {
			return stations[i].StopNumber < stations[j].StopNumber
		}

		return stations[i].LineName < stations[j].LineName
	})

	dict := make(map[string][]Station)
	for _, s := range stations {
		if _, ok := dict[s.Name]; !ok {
			dict[s.Name] = []Station{}
		}
		dict[s.Name] = append(dict[s.Name], s)
	}

	adjList := make(map[string][]Station)
	for i, s := range stations {
		if i-1>=0 {
			if s.LineName == stations[i-1].LineName {
				s.Next = append(s.Next, stations[i-1].Code)
			}
		}
		if i+1<len(stations) {
			if s.LineName == stations[i+1].LineName {
				s.Next = append(s.Next, stations[i+1].Code)
			}
		}
		for _, _s := range dict[s.Name] {
			if _s.Code != s.Code {
				s.Next = append(s.Next, _s.Code)
			}
		}
		if _, ok := adjList[s.Name]; !ok {
			adjList[s.Name] = []Station{}
		}
		adjList[s.Name] = append(adjList[s.Name], s)
	}

	return adjList
}

func generateCodeStationMap(stations map[string][]Station) map[string]Station {
	result := make(map[string]Station)
	for _, v := range stations {
		for _, s := range v {
			result[s.Code] = s
		}
	}


	return result
}
