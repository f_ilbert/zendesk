package app

import (
	. "zendesk/app/solution"
)

type appServiceComponent struct {
	*Solution
}

func NewAppServiceComponent() *appServiceComponent{
	return &appServiceComponent{
		Solution: NewSolution(),
	}
}