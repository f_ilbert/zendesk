package transport

type GetRouteRequest struct {
	Start string `json:"start"`
	End string `json:"end"`
	Time string `json:"time"`
}
